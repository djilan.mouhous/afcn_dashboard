import React from 'react';
import { StatusBar, useColorScheme } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { LoginNavigator, MainNavigator } from './navigation';
import { sampleUser, sampleToken, sampleTheme, themeColors } from './samples';
import { useFonts, Roboto_400Regular, Roboto_700Bold, Roboto_900Black} from '@expo-google-fonts/roboto';


import AppLoading from 'expo-app-loading';
import { AFCNContext } from './store';
import * as GET from './client/get';
import { Helper, Task, Website } from './types';

export default function App() {
  const colorScheme = useColorScheme()
  const [website, setWebsites] = React.useState(new Array<Website>())
  const [theme, setTheme] = React.useState(colorScheme ? themeColors[colorScheme] : themeColors.light)
  const [user, setUser] = React.useState(sampleUser)
  const [helper, setHelpers] = React.useState(new Array<Helper>())
  const [currentHelper, setCurrentHelper] = React.useState(0)
  const [token, setToken] = React.useState(sampleToken)
  const [refresh, setRefresh] = React.useState(false);
  const [loading, setLoading] = React.useState(true)

  const init = React.useRef(true);

  const websiteValue = {website, setWebsites}
  const themeValue = {theme, setTheme}
  const userValue = {user, setUser}
  const helperValue = {helper, setHelpers}
  const currentHelperValue = {currentHelper, setCurrentHelper}
  const refreshValue = { refresh, setRefresh }
  const tokenValue = { token, setToken }
  const loadingValue = { loading, setLoading }

  const appValues = {
    websiteContext: websiteValue,
    themeContext:themeValue,
    userContext: userValue,
    helperContext: helperValue,
    tokenContext: tokenValue,
    loadingContext: loadingValue,
    currentHelperContext: currentHelperValue,
    refreshContext: refreshValue,
  };
  
  React.useEffect(() => {
    if (token !== "") {
      GET.sites(user.nicename,token).then((res: Website[]) => {
        setWebsites(res);
        setLoading(false)  
      });
      
      init.current = false;
    }
  }, [token])

  const [fontsLoaded] = useFonts({
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <AFCNContext.Provider value={appValues}>
      <SafeAreaProvider>
        {
          token !== "" ?
          <MainNavigator />
          :
          <LoginNavigator />
        }
        <StatusBar backgroundColor={"#663399"}/>
      </SafeAreaProvider>
    </AFCNContext.Provider>  
  );
}