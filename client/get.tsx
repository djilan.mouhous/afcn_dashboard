import { Task, Website } from "../types";

export async function taches(website: number, token: string){
    try {
        console.log('fetch api');
        // * fetch api
        let response = await fetch('http://tableau-de-bord-numerique.fr/wp-json/afcn-app/v1/taches/'+website);
        let json = await response.json();
        return json
    }
    catch (e){
        console.log("error fetching taches", e)
        return []
    }
    return[]
}
export async function sites(author : string | null, token: string){
    try {
        console.log('fetch api and author is ', author);
        // * fetch api
            
        let response = await fetch('https://tableau-de-bord-numerique.fr/wp-json/afcn-app/v1/site/'+author);
        //console.log("SITE API", await response.json())
        let json = await response.json();
        return json.slice(0, 2)
    }
    catch (e){
        console.log("Error parsing site", e)
        return []
    }
}

export async function allTaches(sitesArray : Website[], token: string){
    let allTaches : Task[] = []
      try{
        for (let i = 0; i < sitesArray.length; i++) {
            const site = sitesArray[i];
            console.log("step 2", i)
            let newTaches: Task[] | void;
            
            newTaches = await taches(site.id, token)
            if(newTaches !== null && newTaches !== undefined){
                allTaches = allTaches.concat(newTaches)
            }
        } 
        return(allTaches)
    }
    catch(e) {
      return []
    }
}
