import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react'
import LoginScreen from '../screens/loginScreen';
import { HomeNavigator, PublishTaskNavigator, ShopNavigator } from './mainNavigator';

const Stack = createStackNavigator();

export function MainNavigator(){
    return(
    <NavigationContainer>
    <Stack.Navigator initialRouteName='Home' headerMode={"none"}>
        <HomeNavigator/>
        <PublishTaskNavigator/>
        <ShopNavigator/>
    </Stack.Navigator>
    </NavigationContainer>
    )
}

export function LoginNavigator(){
    return(
    <NavigationContainer>
    <Stack.Navigator headerMode={"none"}>
        <Stack.Screen name="Login" component={LoginScreen} />
    </Stack.Navigator>
    </NavigationContainer>
    )
}