import { createStackNavigator } from '@react-navigation/stack';
import React from 'react'
import AllWebsitesScreen from '../screens/allWebsites';
import HomeScreen from '../screens/homeScreen';
import PublishTaskScreen from '../screens/publishTaskScreen';
import PublishWebsiteScreen from '../screens/publishWebsiteScreen';
import ShopScreen from '../screens/shopScreen';
import TaskScreen from '../screens/taskScreen';
import WebsiteScreen from '../screens/websiteScreen';

const Stack = createStackNavigator();

export function HomeNavigator(){
    return(
    <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="AllWebsites" component={AllWebsitesScreen} />
        <Stack.Screen name="Website" component={WebsiteScreen} />
        <Stack.Screen name="PublishWebite" component={PublishWebsiteScreen} />
        <Stack.Screen name="Task" component={TaskScreen} />
    </Stack.Navigator>
    )
}

export function PublishTaskNavigator(){
    return(
    <Stack.Navigator>
        <Stack.Screen name="PublishTask" component={PublishTaskScreen} />
    </Stack.Navigator>
    )
}
export function ShopNavigator(){
    return(
    <Stack.Navigator>
        <Stack.Screen name="Shop" component={ShopScreen} />
    </Stack.Navigator>
    )
}