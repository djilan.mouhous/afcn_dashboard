import { ColorScheme, ThemeColor, ThemeColors, User } from "../types";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Platform, useColorScheme } from "react-native";


async function getUserFromSecuredStore() {
    let idFromSecuredStore
    let userNameFromSecuredStore
    let userNiceNameFromSecuredStore

    idFromSecuredStore = await AsyncStorage.getItem('user_id')
    userNameFromSecuredStore = await AsyncStorage.getItem('user_name')
    userNiceNameFromSecuredStore = await AsyncStorage.getItem('user_nicename')

    const myObj: User = {
        id: Number(idFromSecuredStore) || 0,
        name: userNameFromSecuredStore || "",
        nicename: userNiceNameFromSecuredStore || "",
    }

    return myObj
}

async function getTokenFromSecuredStore() {
    let tokenFromSecuredStore

    tokenFromSecuredStore = await AsyncStorage.getItem('token')

    const token: string = tokenFromSecuredStore || ""

    return token
}

let colorScheme: ColorScheme = "light";


let sampleUser: User = {
    id: 0,
    name: '',
    nicename: ''
}
let sampleToken: string = ""

let themeColors: ThemeColors = {
    dark: {
        main_background : "#222222",
        section_background: "#333333",
        tint_color: "#2980B9",
        bold_color: "#FFFFFF",
        light_color: "#B8B8B8"
    },
    light: {
        main_background : "#F1F2F6",
        section_background: "#FFFFFF",
        tint_color: "#2980B9",
        bold_color: "#000000",
        light_color: "#333333"
    },
}
let sampleTheme: ThemeColor = themeColors[colorScheme]

getUserFromSecuredStore().then((obj: User) => {
    sampleUser = obj
})
getTokenFromSecuredStore().then((t: string) => {
    sampleToken = t
})


export { sampleUser, sampleToken, sampleTheme, themeColors }