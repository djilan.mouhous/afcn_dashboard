import React from "react";
import { sampleUser, sampleTheme } from "../samples";
import { Task, Website, Helper, User, ThemeColors, ThemeColor } from "../types";

export const AFCNContext = React.createContext({
    websiteContext:{
        website: new Array<Website>(),
        setWebsites: (_websites: Website[]) => { }
    },
    userContext:{
        user: sampleUser,
        setUser: (_user: User) => { }
    },
    helperContext:{
        helper: new Array<Helper>(),
        setHelpers: (_helpers: Helper[]) => { }
    },
    tokenContext: {
        token: "",
        setToken: (_token:string) => { }
    },
    loadingContext: {
        loading: false,
        setLoading: (_loading: boolean) => { }
    },
    refreshContext: {
        refresh: false,
        setRefresh: (_refresh: boolean) => { }
    },
    currentHelperContext: {
        currentHelper: 0,
        setCurrentHelper: (_currentHelper: number) => { }
    },
    themeContext: {
        theme: sampleTheme,
        setTheme: (_theme : ThemeColor) => { }
    }
})