export type Website = {
    id: number,
    name: string,
    quota: number,
    quota_current: number,
    quota_done: number,
    quota_remaining: number,
    sub: Array<number>
}
export type Task = {
    id: number,
    name: string,
    time: number,
    status: string,
    image_url: string,
    desc: string,
    website: number,
    chat: Message[],
}
export type User = {
    id: number
    name: string,
    nicename: string,
}
export type Product = {
    id: number,
    name: string,
    image_url: string,
    sub_title: string
    desc: string,
    price_ht: number,
    tax: number
}
export type Subscribe = {
    product: Product,
    duration: number,
    frequence: number
}
export type Message = {
    task_id: number,
    content: string,
    date: Date,
    send_to_user_id: number,
    sent_from_user_id: number,
}
export type Helper = {
    id: number,
    thumbnail_url: string,
    is_new: boolean,
    is_watched: boolean,
    helperContent: HelperContent
}
export type HelperView = {
    background_url: string,
    order: number
}
export type HelperContent = HelperView[]

export type ThemeColor = {
    main_background:  string,
    section_background:  string,
    tint_color: string,
    bold_color: string,
    light_color: string,
}
export type ThemeColors = {
    dark: ThemeColor,
    light: ThemeColor
} 
export type ColorScheme = "dark" | "light"